(function() {
  'use strict';

  angular
    .module('code', ['ngResource', 'ui.router', 'ui.bootstrap', 'toastr', 'slickCarousel']);

})();
