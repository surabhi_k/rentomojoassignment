(function() {
  'use strict';

  angular
    .module('code')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope) {

    $scope.currentSlide = 0
  	$scope.slickConfig = {
	    enabled: true,
	    autoplay: true,
	    draggable: false,
	    autoplaySpeed: 2000,
	    method: {},
      event: {
        afterChange: function (event, slick, currentSlide, nextSlide) {
          $scope.currentSlide = currentSlide;                  
        }
    }
	};

  	$scope.category_list = [
  		{
  			"category_name" : "Men",
  			"category_id" : "01"
  		},
  		{
  			"category_name" : "Women",
  			"category_id" : "02"
  			
  		},
  		{
  			"category_name" : "Kids",
  			"category_id" : "03" 
  		},
  		{
  			"category_name" : "Accessories",
  			"category_id" : "04"  			
  		}
  	];

  	$scope.brandsData = [
  	'https://rukminim1.flixcart.com/merch/240/240/images/1469617467091.jpg?q=90',
  	'https://rukminim1.flixcart.com/merch/240/240/images/1469617417959.jpg?q=90',
  	'https://rukminim1.flixcart.com/merch/240/240/images/1485329785278.png?q=90',
  	'https://rukminim1.flixcart.com/merch/240/240/images/1469617360367.jpg?q=90',
  	'https://rukminim1.flixcart.com/merch/240/240/images/1469616982183.jpg?q=90',
  	'https://rukminim1.flixcart.com/merch/240/240/images/1469617018515.jpg?q=90'
  	]
  	
  	$scope.productData = [
	{
	  "product_name" : "Mast & Harbour Casual Shirt",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/images/style/properties/Tommy-Hilfiger-Men-Shirts_8cc327a44794d9fdccb879b7eae462f8_images_mini.jpg",
	  "product_category" : "Casuals",
	  "product_price" : "11.5",
	  "product_discount" : "0",
	  "product_type" : 1
	}, {
	  "product_name" : "Boltio Casuals",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1548020/2016/10/21/11477030761989-Nike-Women-Casual-Shoes-3211477030761761-1_mini.jpg",
	  "product_category" : "Footwear",
	  "product_price" : "10",
	  "product_discount" : "0",
	  "product_type" : 2
	}, {
	  "product_name" : "Tommy Hilfiger",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1308795/2016/4/15/11460707521772-Hypernation-Men-Waistcoat-9921460707521260-1_mini.jpg",
	  "product_category" : "Apparels",
	  "product_price" : "9.1",
	  "product_discount" : "0",
	  "product_type" : 1
	}, {
	  "product_name" : "Lavie Shoulder Bag",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1713073/2017/1/20/11484900652158-Lavie-Brown-Shoulder-Bag-6831484900651845-1_mini.jpg",
	  "product_category" : "Accessories",
	  "product_price" : "8.6",
	  "product_discount" : "0",
	  "product_type" : 3
	}, {
	  "product_name" : "Shoetopia Heels",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1086323/2015/11/24/11448349935440-Shoetopia-Women-Heels-6691448349934992-1_mini.jpg",
	  "product_category" : "Accessories",
	  "product_price" : "9.8",
	  "product_discount" : "0"
	}, {
	  "product_name" : "Belle Jacket",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1010442/2015/10/5/11444041872433-Belle-Fille-Red-Jacket-7721444041872016-1_mini.jpg",
	  "product_category" : "Apparels",
	  "product_price" : "12",
	  "product_discount" : "0"
	}, {
	  "product_name" : "Kids Dress",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1274267/2016/3/22/11458637136156-Toy-Balloon-kids-Girls-Sea-Green-Fit--Flare-Dress-6441458637135795-1_mini.jpg",
	  "product_category" : "Apparels",
	  "product_price" : "6",
	  "product_discount" : "0"
	}, {
	  "product_name" : "Dressberry Loafers",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1267840/2016/5/23/11464001058886-DressBerry-Women-Black-Quilted-Loafers-1031464001058586-1_mini.jpg",
	  "product_category" : "Footwear",
	  "product_price" : "7.6",
	  "product_discount" : "0"
	}, {
	  "product_name" : "Top",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/932657/2015/12/17/11450327139385-SASSAFRAS-Pink-Top-3391450327138597-1_mini.jpg",
	  "product_category" : "Apparels",
	  "product_price" : "5",
	  "product_discount" : "0"
	}, {
	  "product_name" : "MR Blazer",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/images/style/properties/MR-BUTTON-Men-Blazers_448e3ea99f35faa92ca3922460c40c22_images_mini.jpg",
	  "product_category" : "Apparels",
	  "product_price" : "11",
	  "product_discount" : "0"
	}, {
	  "product_name" : "Forever Sun Hat",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1575213/2016/10/26/11477460859729-FOREVER-21-Women-Black--Off-White-Patterned-Sun-Hat-5811477460859395-1_mini.jpg",
	  "product_category" : "Accessories",
	  "product_price" : "5.6",
	  "product_discount" : "0"
	}, {
	  "product_name" : "Mast & Harbour Casual Shirt",
	  "product_img" : "http://assets.myntassets.com/h_240,q_95,w_180/v1/assets/images/1707908/2016/12/29/11483009004027-LOCOMOTIVE-Men-Shirts-9961483009003719-1_mini.jpg",
	  "product_category" : "Casuals",
	  "product_price" : "9.4",
	  "product_discount" : "0"
	}]
  }
})();
